<?php

/**
 * Written by Dominik Dafert
 * Fixes von Paul Chameloher
 * Selbsthostlösung
 * Version: 1.2
 *
 *
 * Bitte lege ein keys.php file an
 */

require_once (__DIR__ . '/keys.php');

//-------------------------------------------------------------------------------------

$url = "https://". $server .".webuntis.com/WebUntis/?school=".$school;

$opts = array(
  'http'=>array(
    'method'=>"GET"
  )
);
$context = stream_context_create($opts);
file_get_contents($url, false, $context);
$cookies = array();
foreach ($http_response_header as $hdr) {
  if (preg_match('/^Set-Cookie:\s*([^;]+)/', $hdr, $matches)) {
    parse_str($matches[1], $tmp);
    $cookies += $tmp;
  }
}
$sessionID = $cookies["JSESSIONID"];
$schoolnameCookie =  str_replace('"', '', $cookies['schoolname']);

$datum = date('Y-m-d', strtotime(' + 1 days')); // Aufbau des Datums für den iCal-Kalender, Plus 1 Tag damit am Sonntag die Nachricht für den neuen Stundenplan am Montag kommt.
$url = "https://".$server.".webuntis.com/WebUntis/Ical.do?elemType=". $elemType ."&elemId=". $elemID ."&rpt_sd=". $datum;
setlocale(LC_TIME, array('de_DE.UTF-8','de_DE@euro','de_DE','german'));

$data = array('j_username' => $username, 'j_password' => $password, 'school' => $school);
$stream = array(
  'http' => array(
    'method' =>     'POST',
    'header' =>
      "Accept: application/json\r\n" .
      "Connection: close\r\n".
      "Content-type: application/x-www-form-urlencoded\r\n" .
      "Cookie: JSESSIONID=".$sessionID."\r\n" .
      "Cookie: schoolname=".$schoolnameCookie,

    'content' => http_build_query($data)
  )
);
$context = stream_context_create($stream);
file_get_contents("https://". $server .".webuntis.com/WebUntis/j_spring_security_check", false, $context);

$opts = array(
  'http'=>array(
    'method'=>"GET",
    'header'=>"Cookie: JSESSIONID=".$sessionID.""
  )
);
$context = stream_context_create($opts);
$file = file_get_contents($url, false, $context);

if ($file === false) {
  echo "Fehler";
  die();
}

echo $file;

?>